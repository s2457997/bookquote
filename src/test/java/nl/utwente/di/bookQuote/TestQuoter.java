package nl.utwente.di.bookQuote;

import org.junit.Assert;
import org.junit.Test;

public class TestQuoter
{
    @Test
    public void testBook1() throws Exception
    {
        Quoter quoter = new Quoter();
        double celsius = quoter.getBookPrice("10");
        Assert.assertEquals("temperature" , 50.0, celsius , 0.0);
    }
}